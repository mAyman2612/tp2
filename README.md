# Travail pratique 2 - INF1070

Ce court document contient l'énoncé du travail pratique 2 donné dans le cadre du cours **INF1070 - Utilisation et administration des systèmes informatiques** au trimestre d'**hiver 2021**, enseigné par Mélanie Lord, Johnny Tsheke Shele et Moussa Abdenbi, à l'Université du Québec à Montréal. Ce TP est commun aux 3 groupes. 
Le but de ce TP est de s'initier et d'expérimenter certaines tâches d'un administrateur système.

**Le travail doit être réalisé individuellement (pas d'équipe)**.

## Présentation

Ce travail porte principalement sur l'installation, la configuration et l'utilisation d'une application au travers de la technologie de conteneurisation `Docker`.
Cette application, nommée `s`, est un agrégateur de moteurs de recherche.
Elle offre une interface en ligne de commande, et une interface web.
Cet énoncé explique les étapes que vous devez effectuer pour installer l'application `s` à partir de son code source.

Grâce à la technologie `Docker` vue en cours, vous allez lancer 3 versions de cette application en même temps.
Pour ce faire, vous devez, notamment,
 - créer une image `Docker` pour l'application, grâce à un `Dockerfile`;
 - configurer et lancer plusieurs conteneurs pour utiliser différentes interfaces de l'application. 

```mermaid
graph TB
  subgraph "Application s"
  SubGraph1Flow(Mode Console)
  SubGraph1Flow -- Direct --> s-commande
  SubGraph1Flow -- Interactif --> s-console
  SubGraph1Flow2(Mode Web)
  SubGraph1Flow2 -- HTTPS --> s-web
  SubGraph1Flow3(Configuration)
  SubGraph1Flow3 --> SubGraph1Flow
  SubGraph1Flow3 --> SubGraph1Flow2
  SubGraph1Flow4(Image Docker)
  SubGraph1Flow4 --> SubGraph1Flow3  
end
```

> :warning: **Attention** :warning: Assurez-vous d'avoir assez d'espace disque dans votre machine virtuelle avant de commencer ce TP. 
Il faut avoir au **minimum 6 Go de libre**.

## Travail à réaliser

Tout d'abord téléchargez l'archive du TP.
```
wget https://gitlab.info.uqam.ca/inf1070/tp2-hiver-2021/-/raw/master/TP2.tar
```
Désarchivez le fichier et déplacez-vous à l'intérieur du répertoire désarchivé.
```
tar -xvf TP2.tar
cd TP2
```
Dans ce répertoire, il y a 2 sous-répertoires :
```
TP2
 ├── remise
 └── s
```
- Le code source de l'application que vous devez installer se trouve dans le répertoire `s` et [+ sa documentation +] se trouve dans le fichier `s/README.md`. 
- L'ensemble des [+ fichiers que vous devez remplir et remettre +] se trouve dans le répertoire `remise`.
    - Un fichier `Dockerfile` pour créer l'image de l'application.
    - Un fichier `config` pour configurer l'application.
    - Un fichier `bashrc` pour configurer le _shell_ de la version en mode terminal de l'application.
    - Un certificat et une clé pour l'encryptage `https` de la version web de l'application.
    - Un rapport `rapport-tp2.md` où vous devez **écrire et expliquer toutes les commandes** que vous aurez faites.
        - Vous devez notamment y détailler l'objectif et le fonctionnement de toutes vos commandes.
    - Des captures d'écran.

> :warning: **Attention** :warning:
>- Dans ce qui suit, vous devez vous placer dans le répertoire `TP2` en tout temps.
>- Toutes les commandes `sudo docker` (`sudo docker build`, `sudo docker run`, `sudo docker create`, etc.) sont à compléter.
>- Dans la suite, pour que vos commandes fonctionnent correctement (surtout à la correction!) à chaque fois que vous devez monter des fichiers (avec l'option `--mount`), utilisez un chemin aboslu avec `$(pwd)` pour la source.

### Création de l'image Docker

Commencez par créer une image qui contiendra l'application et tout ce qu'il lui est nécessaire pour fonctionner.

Pour ce faire, éditez le fichier `remise/Dockerfile` fourni dans l'archive, et ajoutez les directives adéquates pour effectuer les tâches suivantes :
- Mettre à jour les paquets de l'image avec la commande `apt update` et installez le paquet `elinks`.
    - Assurez-vous qu'il n'y ait **aucune interaction** lors de la mise à jour et de l'installation, autrement la construction de l'image échouera.
- Copier le contenu du répertoire de l'application `s` dans le répertoire `/go/src/github.com/zquestz/s` de l'image.
- Définir le répertoire `/go/src/github.com/zquestz/s` comme répertoire de travail par défaut de l'image.
- Compiler et installer l'application `s`.
    - Lire la documentation de l'application `s` pour trouver les commandes à exécuter.
- Créer le répertoire `/root/.config/s`.

Une fois le fichier `remise/Dockerfile` prêt, lancez la construction de l'image, que vous nommez `inf1070-tp2`, avec `sudo docker build`

<p>

<details>

<summary>Vérification</summary>

Pour vérifier que ça s'est bien exécuté, tapez la commande `sudo docker images` et la sortie devrait ressembler à ça

<pre>
REPOSITORY              TAG                 IMAGE ID            CREATED              SIZE
inf1070-tp2             latest              7aa38ce96c9a        About a minute ago   1.06GB
moussaabdenbi/inf1070   tp2-h2021           0b59414b5a2a        9 days ago           1.04GB
</pre>

Ça doit au moins contenir les 2 images plus haut, il peut y en avoir d'autres. 
Ne faites pas attention aux `IMAGE ID` qui peuvent varier d'une machine à l'autre.

</details>

</p>

### Configuration

Dans cette section vous allez personnaliser l'application `s`.

- Éditez le fichier texte `remise/config` en respectant le format [JSON](https://fr.wikipedia.org/wiki/JavaScript_Object_Notation) pour ajouter les spécifications suivantes.
    - D'abord, lisez la documentation de l'application `s` pour savoir comment définir un fournisseur (en anglais _provider_) par défaut et comment ajouter un nouveau fournisseur.
    - Ensuite, dans le fichier `remise/config` configurez le moteur de recherche `duckduckgo` comme fournisseur par défaut de l'application `s`. Ce moteur de recherche est présent par défaut dans l'application.
    - Ajoutez un nouveau fournisseur `radio` pour le site d'information Radio-Canada. Pour cela :
        - Allez sur le site de Radio-Canada https://ici.radio-canada.ca/
        - Repérez la barre de recherche, et faites une recherche pour `covid`, par exemple.
        - Dans l'URL de la page de résultats, identifiez la paire attribut-valeur de la requête de recherche pour `covid`.
        - Dans le fichier `remise/config`, créez une nouvelle entrée `radio`, dans `customProviders`, pour ajouter Radio-Canada.
        - Utilisez le _tag_ `news` pour cet ajout.
- Éditez le fichier texte `remise/bashrc` avec les spécifications suivantes :
    - Créez un alias nommé `s` qui lance la commande `s` avec le binaire `elinks` comme navigateur pour afficher les résultats des recherches.
        - Lisez la documentation de la commande `s` dans le fichier `s/README.md` pour savoir comment vous pouvez choisir le fichier binaire à exécuter pour afficher les résultats d'une recherche.
    - Créez un autre alias `sr` pour une recherche avec le fournisseur `radio`, créé précédemment.
    - Lisez la documentation de l'application `s`, et ajoutez ce qu'il faut pour activer l'autocomplétion pour la commande `s`.

### Mode console

Dans cette partie, vous allez créer 2 versions de l'application `s` : `s-commande` et `s-console`.

#### Conteneur pour une exécution directe de l'application

En utilisant la commande `sudo docker run` lancez un conteneur, nommé `s-commande`, avec l'image `inf1070-tp2` et les spécifications suivantes :
- Le conteneur lance la commande `/go/bin/s` et les arguments de sa recherche, puis s'arrête tout de suite après.
- Vous devez monter avec `--mount` le **fichier** `remise/config` dans le fichier `/root/.config/s/config` en **lecture seule** (en anglais _readonly_). **Attention** c'est un fichier que vous devez monter et non pas un **répertoire**.
- Le conteneur va lancer la commande `s` avec les options suivantes :
    - `elinks` comme navigateur pour afficher les résultats
    - le fournisseur `radio` (faites un test avec `covid`)

<p>

<details>

<summary>Vérification</summary>

Si vous lancez correctement le conteneur avec le bon montage et les bonnes options pour la commande `s`, pour une recherche du mot-clé `covid` sur le fournisseur `radio`, sur votre terminal vous verez quelque chose de similaire à ça :

<pre>
   Link: [1]manifest
   Link: [2]prefetch
   Link: [3]preload
   Nous utilisons les temoins de navigation (cookies) afin d'operer et
   d'ameliorer nos services ainsi qu'`a des fins publicitaires. Le respect de
   votre vie privee est important pour nous. Si vous n'etes pas `a l'aise
   avec l'utilisation de ces informations, veuillez revoir vos parametres
   avant de poursuivre votre visite.
   [4]IFrame

[...]

                       Filtre [79]_____________________
                          [80]Effacer le contenu saisi
                                 [81]Rechercher

                       Resultats de recherche pour covid

Emissions

     [82]COVID-19 : Le point (Nouvelle fenetre)

  COVID-19 : Le point (Nouvelle fenetre)

     A la memoire des victimes de la Covid-19 (Nouvelle fenetre)

  A la memoire des victimes de la Covid-19 (Nouvelle fenetre)

Resultats pour << covid >>

   Resultats de 1 `a 20 sur 10000
   Affichage :
   [83][ _________________________________ ]

[84]Resultats pour << covid >>

     [85]Le cauchemar de l'apres COVID-19
     [86]Une femme pose devant sa residence.

  [87]Le cauchemar de l'apres COVID-19

     Une femme de Saint-Ludger-de-Milot qui a contracte la COVID-19 deplore
     l`absence de soins post-COVID.

        26 mars
        [88]Sante publique

     [89]COVID-19 : la limule menacee?
     [90]Deux limules

[...]
</pre>

</details>

</p>

#### Conteneur pour une exécution interactive de l'application

- En utilisant la commande `sudo docker create` créez un conteneur nommé `s-console` avec l'image `inf1070-tp2` que vous avez créé précédemment, et les spécifications suivantes :
    - Le conteneur doit être interactif et doit lancer un terminal.
    - Vous devez monter avec `--mount` le **fichier** `remise/config` dans le fichier `/root/.config/s/config` en **lecture seule**.
    - De même, vous devez monter, avec un autre `--mount`, le **fichier** `remise/bashrc` dans le fichier `/root/.bashrc` en **lecture seule** aussi.
    - Le conteneur va lancer la commande `/bin/bash`.
- Démarrez le conteneur `s-console` avec la commande `sudo docker start` et attachez-vous à ce conteneur avec `sudo docker attach`.
- Vérifiez que les aliases sont bien présents, que l'autocomplétion est activée, et que le fournisseur `radio` est fonctionnel.
<p>

<details>

<summary>Vérification</summary>

Si vous avez correctement créé le conteneur, exécutez la commande `sudo docker ps -a` et vous aurez un affichage similaire à ça :

<pre>
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
56cede8cf555        inf1070-tp2         "/bin/bash"         3 minutes ago       Created                                 s-console
</pre>

Quand vous aurez démarré votre conteneur, la commande `sudo docker ps` vous affiche :

<pre>
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
56cede8cf555        inf1070-tp2         "/bin/bash"         3 minutes ago       Created                                 s-console
</pre>

Quand vous vous rattachez au conteneur `s-console`, vous aurez un affichage similaire à ça :

<pre>
root@5a23d17a973c:/go/src/github.com/zquestz/s#
</pre>

Maintenant, pour vérifier l'autocomplétion, si vous tapez la commande `s -p g`, par exemple, et vous tapez 2 fois sur la touche de tabulation (`TAB`) vous aurez un affichage similaire à ça :

<pre>
root@5a23d17a973c:/go/src/github.com/zquestz/s# s -p g
giphy       gist        github      gmail       go          godoc       goodreads   google      googledocs  googleplus
</pre>

Si vous faites une recherche avec `sr covid`, vous aurez un affichage similaire à ça :

<pre>
root@5a23d17a973c:/go/src/github.com/zquestz/s# sr covid
   Link: [1]manifest
   Link: [2]prefetch
   Link: [3]preload
   Nous utilisons les temoins de navigation (cookies) afin d'operer et
   d'ameliorer nos services ainsi qu'`a des fins publicitaires. Le respect de
   votre vie privee est important pour nous. Si vous n'etes pas `a l'aise
   avec l'utilisation de ces informations, veuillez revoir vos parametres
   avant de poursuivre votre visite.
   [4]IFrame

[...]

                       Filtre [79]_____________________
                          [80]Effacer le contenu saisi
                                 [81]Rechercher

                       Resultats de recherche pour covid

Emissions

     [82]COVID-19 : Le point (Nouvelle fenetre)

  COVID-19 : Le point (Nouvelle fenetre)

     A la memoire des victimes de la Covid-19 (Nouvelle fenetre)

  A la memoire des victimes de la Covid-19 (Nouvelle fenetre)

Resultats pour << covid >>

   Resultats de 1 `a 20 sur 10000
   Affichage :
   [83][ _________________________________ ]

[84]Resultats pour << covid >>

     [85]Le cauchemar de l'apres COVID-19
     [86]Une femme pose devant sa residence.

  [87]Le cauchemar de l'apres COVID-19

     Une femme de Saint-Ludger-de-Milot qui a contracte la COVID-19 deplore
     l`absence de soins post-COVID.

        26 mars
        [88]Sante publique

     [89]COVID-19 : la limule menacee?
     [90]Deux limules

[...]
</pre>

Rappel `sr` est un alias pour une recherche avec le fournisseur `radio`.

</details>

</p>

#### Question

À votre avis, si l'on avait monté le fichier `bashrc` dans le premier conteneur `s-commande` (comme ce qu'on a fait dans le deuxième conteneur `s-console`), est-ce qu'on pourrait utiliser l'alias `sr`? Expliquez pourquoi. (Répondez dans le fichier `rapport-tp2.md`)

### Mode web

Dans cette partie, vous allez créer une version web de l'application `s` avec un accès en `https`.

- En utilisant la commande `sudo docker create` créez un conteneur nommé `s-web` avec l'image `inf1070-tp2` que vous avez créé précédemment, et selon les spécifications suivantes :
    - Vous devez monter avec `--mount` les **fichiers** suivants en **lecture seule** :
        - le fichier `remise/config` dans le fichier `/root/.config/s/config`
        - le certificat `remise/cert.pem` dans le fichier `/root/cert.pem`
        - la clé `remise/key.pem` dans le fichier `/root/key.pem`
    - Le conteneur va lancer la commande `s` en mode serveur avec le certificat et la clé fraichement montés.
        - Le serveur devra se lancer sur le port `4XYZ`, où `XYZ` correspond aux 3 derniers chiffres de votre code permanent.
        - Par exemple, si votre code permanent est `ABCD12345678` alors `XYZ` vaut `678`.
    - Publiez le port `4XYZ` du conteneur avec le port `8XYZ` de votre machine hôte.
- Démarrez le conteneur `s-web` avec la commande `sudo docker start`.
- Ouvrez un fureteur, et allez à l'adresse `https://localhost:8XYZ`, et vérifiez que vous avez bien accès au serveur de l'application `s` et que le fournisseur `radio` est disponible. 

<p>

<details>

<summary>Vérification</summary>

Si vous avez correctement créé le conteneur, exécutez la commande `sudo docker ps -a`, et vous aurez un affichage similaire à ça :

<pre>
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
e179e8841084        inf1070-tp2         "********************"   9 seconds ago       Created                                 s-web
</pre>

Quand vous aurez démarré votre conteneur, la commande `sudo docker ps` vous affiche :

<pre>
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
e179e8841084        inf1070-tp2         "********************"   38 seconds ago      Up 12 seconds       0.0.0.0:8789->4789/tcp   s-web
</pre>
Dans cet exemple `XYZ = 789`.

Évidemment, vous ne verrez pas les `"***********"` vous verrez vos commandes à vous!

Quand vous ouvrirez un fureteur à l'adresse `https://localhost:8779` sur votre machine, vous tomberez sur une page d'avertissement (comme celle plus bas).
Cliquez sur les boutons `Avancez...` puis `Acceptez le risque et poursuivre`.

<img src="img/page-2.png" alt="Page 1">

Vous devriez être redirigé vers la page du serveur de l'application `s`. Vous verrez une page similaire à celle-ci (remarquez que l'URL est en `https`, et qu'en haut à droite, le fournisseur par défaut est bien `duckduckgo`) :

<img src="img/page-3.png" alt="Page 2">

Vous pouvez vérifier que `radio`, que vous avez ajouter, est bien présent dans la liste des fournisseurs.

<img src="img/page-4.png" alt="Page 3">

</details>

</p>

### Captures d'écran

Vous devez fournir les **2 captures d'écran** suivantes :
- Une capture d'écran de votre terminal après avoir exécutez les commandes `sudo docker images` suivi de `sudo docker ps -a`.
    - Votre terminal doit, minimalement, afficher les images `moussaabdenbi/inf1070` et `inf1070-tp2` ainsi que les conteneurs `s-commande`, `s-console` et `s-web` (ou, du moins, les conteneurs que vous avez réussi à lancer).
- Une capture d'écran avec votre fureteur ouvert sur le serveur web de l'application `s`.
    - L'adresse en `https` avec votre numéro de port personnalisé doit être bien visible.

Mettez ces captures d'écran dans le répertoire `remise/`.
<p>

<details>

<summary>Exemples de captures d'écran</summary>

La capture d'écran de votre terminal devrait ressembler à ça :

<img src="img/terminal.png" alt="Terminal">

La capture d'écran de votre fureteur devrait ressembler à ça :

<img src="img/page-3.png" alt="Fureteur">

</details>

</p>

## Modalités de remise

- Vous devez remettre l'archive `remise-tp2.tar`.
    - Cette archive contiendra le répertoire `remise/`.
    - Avant d'archiver ce répertoire, assurez-vous que les fichiers `remise/Dockerfile`, `remise/config`, `remise/bashrc` et `remise/rapport-tp2.md` sont bien présents et remplis, ainsi que les captures d'écran et les deux fichiers `cert.pem` et `key.pem`.
- Votre travail doit être remis au plus tard le **30 avril 2021 à 23h55** par l'intermédiaire de la plateforme [Moodle](https://www.moodle.uqam.ca/).
- Une pénalité de **2 points** par heure de retard sera appliquée, jusqu'à **un maximum** de 24 heures. Après ce délai, aucun TP ne sera accepté.
- **Aucune remise par courriel ne sera acceptée**, peu importe le motif. 
    - Plus précisément, si vous envoyez votre travail par courriel, il sera considéré comme non remis. 
    - Il est donc de votre responsabilité de vous assurer d'être capable de faire une remise à temps par l'intermédiaire de Moodle.
- **Aucune réponse** ne sera réservée à une personne qui envoie le TP par courriel.

### Barème de correction

Le travail est corrigé sur 100 points selon le barème suivant.

- **Fichiers.**
    - Le fichier `Dockerfile` complet et fonctionnel. [- 10 points -]
    - Le fichier `config` complet et avec les bonnes directives pour l'application `s`. [- 7.5 points -]
    - Le fichier `bashrc` avec les bonnes lignes de commandes. [- 7.5 points -]
    - Les captures d'écran de votre terminal et de votre fureteur. [- 5 points -]
    - Le fichier `rapport-tp2.md` avec, entre autres, toutes les sections de l'énoncé remplies, avec des explications claires et pertinentes.  [- 35 points -]
- **Fonctionnement.** Les commandes tirées de `rapport-tp2.md` sont fonctionnelles et reproduisibles pour les cas suivants.
    - Exécution directe de l'application [- 10 points -]
    - Exécution interactive de l'application [- 10 points -]
    - Exécution web de l'application [- 10 points -]
- Appréciation globale sur la qualité générale des fichier remis (format du fichier, utilisation de Markdown, qualité des phrases et fautes de français s'il y a lieu, etc.). [- 5 points -]


## Documentation

Les bonnes pratiques de l'écriture d'un `Dockerfile` voir [ici](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/)

### Directives `Dockerfile`

- [WORKDIR](https://docs.docker.com/engine/reference/builder/#workdir)
- [COPY](https://docs.docker.com/engine/reference/builder/#copy)
- [RUN](https://docs.docker.com/engine/reference/builder/#run)

### Commandes docker

- [`docker build`](https://docs.docker.com/engine/reference/commandline/build/)
- [`docker run`](https://docs.docker.com/engine/reference/commandline/run/)
- [`docker create`](https://docs.docker.com/engine/reference/commandline/create/)
- [`docker start`](https://docs.docker.com/engine/reference/commandline/start/)
- [`docker stop`](https://docs.docker.com/engine/reference/commandline/stop/)
